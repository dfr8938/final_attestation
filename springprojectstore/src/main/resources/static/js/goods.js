const btnsEdit = document.querySelectorAll("#btn_edit"),
    formsGoods = document.querySelectorAll("#form_goods"),
    btnsReset = document.querySelectorAll("#btn_reset"),
    btnsClose = document.querySelectorAll("#btn_close"),
    btnsDisable = document.querySelectorAll("#btn_disable"),
    btnsUnDisable = document.querySelectorAll("#btn_un_disable");

// for (let i = 0; i < btnsDisable.length; i++) {
//     btnsDisable[i].setId = `btn_disable_${i + 1}`;
//     btnsUnDisable[i].setId = `btn_un_disable_${i + 1}`;
// }
//
// for (let i = 0; i < btnsDisable.length; i++) {
//     btnsDisable[i].addEventListener('click', () => {
//         btnsDisable[i].style.display = 'none';
//         btnsUnDisable[i].style.display = 'flex';
//     })
// }
//
// for (let i = 0; i < btnsUnDisable.length; i++) {
//     btnsUnDisable[i].addEventListener('click', () => {
//         btnsUnDisable[i].style.display = 'none';
//         btnsDisable[i].style.display = 'flex';
//     })
// }

for (let i = 0; i < formsGoods.length; i++) {
    formsGoods[i].setId = `form_goods${i + 1}`;
}

for (let i = 0; i < btnsEdit.length; i++) {
    btnsEdit[i].setId = `btn_edit_${i + 1}`;
    btnsEdit[i].addEventListener('click', () => {
        formsGoods[i].style.display = 'flex';
    });
}

for (let i = 0; i < btnsClose.length; i++) {
    btnsClose[i].id = `btn_close_${i + 1}`;
    btnsClose[i].addEventListener('click', () => {
        formsGoods[i].style.display = 'none';
    });
}

for (let i = 0; i < btnsReset.length; i++) {
    btnsReset[i].id = `btn_reset_${i + 1}`;
    let title = document.querySelectorAll("#title"),
        descFull = document.querySelectorAll("#desc_full"),
        descPromo = document.querySelectorAll("#desc_promo"),
        price = document.querySelectorAll("#price"),
        quantity = document.querySelectorAll("#quantity");
    title[i].setId = `title_${i + 1}`;
    descFull[i].setId = `descFull_${i + 1}`;
    descPromo[i].setId = `descPromo_${i + 1}`;
    price[i].setId = `price_${i + 1}`;
    quantity[i].setId = `quantity_${i + 1}`;
    btnsReset[i].addEventListener('click', () => {
        title[i].value = "";
        descFull[i].innerHTML = "";
        descPromo[i].innerHTML = "";
        price[i].value = "";
        quantity[i].value = "";
    });
}


