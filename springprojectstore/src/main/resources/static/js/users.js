const btnsEdit = document.querySelectorAll("#btn_edit"),
        formsUser = document.querySelectorAll("#form_user"),
        btnsClose = document.querySelectorAll("#btn_close");

for (let i = 0; i < formsUser.length; i++) {
    formsUser[i].id = `form_user${i + 1}`;
}

for (let i = 0; i < btnsEdit.length; i++) {
    btnsEdit[i].id = `btn_edit_${i + 1}`;
    btnsEdit[i].addEventListener('click', () => {
       formsUser[i].style.display = 'flex';
    });
}

for (let i = 0; i < btnsClose.length; i++) {
    btnsClose[i].id = `btn_close_${i + 1}`;
    btnsClose[i].addEventListener('click', () => {
        formsUser[i].style.display = 'none';
    });
}