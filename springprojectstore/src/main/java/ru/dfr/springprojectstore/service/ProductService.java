package ru.dfr.springprojectstore.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.dfr.springprojectstore.entity.Product;
import ru.dfr.springprojectstore.entity.User;
import ru.dfr.springprojectstore.repository.ProductRepository;

import java.util.*;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final UserService userService;

    public List<Product> findAll() {
        try {
            if (productRepository.findAll().size() == 0) {
                return null;
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
        return productRepository.findAll();
    }

    public Product findById(Integer id) {
        Optional<Product> optionalProduct = productRepository.findById(id);
        return optionalProduct.orElseGet(Product::new);
    }

    public void saveProduct(Product product) {
        productRepository.save(product);
    }

    public void removeProduct(Integer productId) {
        productRepository.deleteById(productId);
    }

    public List<Product> findProductByPriceAfterAndPriceBefore(Integer price, Integer price2) {
        return productRepository.findProductByPriceAfterAndPriceBeforeOrderByPrice(price, price2);
    }

    public void addProductToUser(Integer userId, Integer productId, Integer orderNum) {
        User user = userService.findById(userId);
        Product product = productRepository.findById(productId).orElse(null);
        assert product != null;
        product.setUser(user);
        if (orderNum == product.getQuantity()) {
            product.setQuantity(0);
            product.setOrders(orderNum);
        } else if (product.getQuantity() > 0 && orderNum > product.getQuantity()) {
            product.setOrders(product.getQuantity() + product.getOrders());
            product.setQuantity(0);
        } else if (product.getQuantity() > 0 && orderNum < product.getQuantity()) {
            product.setOrders(product.getOrders() + orderNum);
            product.setQuantity(product.getQuantity() - orderNum);
        } else {
            product.setOrders(0);
        }
        productRepository.save(product);
    }

    public Map<String, Integer> countsGoodsByCategory(List<Product> goods) {
        Map<String, Integer> result = new HashMap<>();
        List<String> categories = goods.stream().map(Product::getCategory).collect(Collectors.toList());
        for (String category : categories) {
            if (!result.containsKey(category)) {
                result.put(category, 1);
            } else {
                result.put(category, result.get(category) + 1);
            }
        }
        return result;
    }

    public Map<String, String> categoryAndUrl(List<Product> goods) {
        Map<String, String> result = new HashMap<>();
        for (int i = 0; i < goods.size(); i++) {
            result.put(goods.get(i).getCategory(), goods.get(i).getLinkToCategory());
        }
        return result;
    }

    public Map<String, String> typeAndUrl(List<Product> goods) {
        Map<String, String> result = new HashMap<>();
        for (int i = 0; i < goods.size(); i++) {
            result.put(goods.get(i).getType(), goods.get(i).getLinkToCategory() + goods.get(i).getLinkToType());
        }
        return result;
    }

    public List<Product> findAllByUserId(Integer userId) {
        return productRepository.findAllByUser_Id(userId);
    }

    public List<Product> findByTitleEndsWith(String str) {
        return productRepository.findByTitleEndsWith(str);
    }

    public List<Product> findByIsActive(Integer isActive) {
        return productRepository.findByIsActive(isActive);
    }
}
