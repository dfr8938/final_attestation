package ru.dfr.springprojectstore.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.dfr.springprojectstore.entity.Product;
import ru.dfr.springprojectstore.entity.Purchase;
import ru.dfr.springprojectstore.entity.User;
import ru.dfr.springprojectstore.repository.PurchaseRepository;

import java.util.List;

@Component
@RequiredArgsConstructor
public class PurchaseService {

    private final PurchaseRepository purchaseRepository;
    private final ProductService productService;
    private final UserService userService;

    public List<Purchase> getPurchases() {
        return purchaseRepository.findAll();
    }

    public void deletePurchase(Integer id) {
        purchaseRepository.deleteById(id);
    }

    public Purchase getPurchase(Integer id) {
        return purchaseRepository.findById(id).orElseGet(Purchase::new);
    }

    public void savePurchase(Purchase purchase) {
        purchaseRepository.save(purchase);
    }

    public void addPurchase(Integer productId, Integer userId, Integer orders) {
        Product product = productService.findById(productId);
        User user = userService.findById(userId);
        Purchase purchase = Purchase.builder()
                .orders(orders)
                .user(user)
                .product(product)
                .build();
        purchaseRepository.save(purchase);
    }

    public List<Purchase> listPurchasesByUserId(Integer userId) {
        return purchaseRepository.findAllByUser_Id(userId);
    }

    public List<Purchase> listPurchasesByProductId(Integer productId) {
        return purchaseRepository.findAllByProduct_Id(productId);
    }
}
