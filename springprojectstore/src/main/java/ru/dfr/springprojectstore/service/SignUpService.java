package ru.dfr.springprojectstore.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.dfr.springprojectstore.entity.User;
import ru.dfr.springprojectstore.repository.UserRepository;
import ru.dfr.springprojectstore.view.UserView;

@Component
@RequiredArgsConstructor
public class SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final UserView userView;

    public void signUpUser(User user) {
        User newUser = User.builder()
                .email(user.getEmail())
                .password(passwordEncoder.encode(user.getPassword()))
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .role(User.Role.USER)
                .build();
        userRepository.save(newUser);
    }
}
