package ru.dfr.springprojectstore.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.dfr.springprojectstore.entity.Product;
import ru.dfr.springprojectstore.entity.User;
import ru.dfr.springprojectstore.exception.UserNotFoundException;
import ru.dfr.springprojectstore.repository.ProductRepository;
import ru.dfr.springprojectstore.repository.UserRepository;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final ProductRepository productRepository;
    private final PasswordEncoder passwordEncoder;

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findById(Integer id) {
        return userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public void deleteById(Integer id) {
        userRepository.deleteById(id);
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(UserNotFoundException::new);
    }

    public void updateUser(User user) {
        userRepository.save(user);
    }

    public void addProductToUser(Integer userId, Integer productId) {
        User user = userRepository.getById(userId);
        Product product = productRepository.getById(productId);
    }

    public void createUser(User user) {
        User newUser = User.builder()
                .email(user.getEmail())
                .password(passwordEncoder.encode(user.getPassword()))
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .role(User.Role.USER)
                .build();
        userRepository.save(newUser);
    }
}
