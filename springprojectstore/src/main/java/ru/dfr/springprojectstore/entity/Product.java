package ru.dfr.springprojectstore.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Check;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "goods")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String title;
    private String descriptionFull;
    private String descriptionPromo;
    private Integer price;
    private String urlImage;
    private Integer quantity;

    private String category;
    private String linkToCategory;
    private String type;
    private String linkToType;

    @Column(name = "is_active", columnDefinition = "integer default 1")
    private Integer isActive = 1;

    @Column(columnDefinition="Integer default '0'")
    private Integer orders;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany
    private List<Purchase> purchase;
}
