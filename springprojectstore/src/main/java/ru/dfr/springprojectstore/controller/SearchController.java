package ru.dfr.springprojectstore.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.dfr.springprojectstore.entity.Product;
import ru.dfr.springprojectstore.entity.User;
import ru.dfr.springprojectstore.service.ProductService;
import ru.dfr.springprojectstore.service.UserService;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class SearchController {

    private final ProductService productService;
    private final UserService userService;

    @GetMapping("/search_page")
    public String resultSearch(@RequestParam("search") String search, Model model, Principal principal) {
        return modelsToURL(search, "search_page", model, principal, productService, userService);
    }

    @GetMapping("/search")
    public String getSearchProducts(@RequestParam("search") String search, Model model, Principal principal) {
        return modelsToURL(search, "search_page", model, principal, productService, userService);
    }

    @GetMapping("/search_price")
    public String resultSearchPrice(Model model, Principal principal, @RequestParam("price") Integer price,
                                    @RequestParam("price2") Integer price2) {
        List<Product> goods = productService.findAll();
        model.addAttribute("countAllProducts", productService.findByIsActive(1).size());
        model.addAttribute("counts", productService.countsGoodsByCategory(productService.findByIsActive(1)));
        model.addAttribute("categories", productService.categoryAndUrl(productService.findByIsActive(1)));
        model.addAttribute("types", productService.typeAndUrl(productService.findProductByPriceAfterAndPriceBefore(price, price2)));
        model.addAttribute("goods", productService.findProductByPriceAfterAndPriceBefore(price, price2));
        model.addAttribute("user", activeUser(principal, userService));
        return "/search_price";
    }

    private static String modelsToURL(String search, String url, Model model, Principal principal, ProductService productService, UserService userService) {
        model.addAttribute("countAllProducts", productService.findByIsActive(1).size());
        model.addAttribute("counts", productService.countsGoodsByCategory(productService.findByIsActive(1)));
        model.addAttribute("categories", productService.categoryAndUrl(productService.findByIsActive(1)));
        model.addAttribute("types", productService.typeAndUrl(productService.findByIsActive(1)));
        model.addAttribute("goods", searchList(search, productService).stream().distinct().collect(Collectors.toList()));
        model.addAttribute("user", activeUser(principal, userService));
        return "/" + url;
    }

    private static List<Product> searchList(String search, ProductService productService) {
        List<Product> goods = productService.findByIsActive(1);
        List<Product> searchGoods = new ArrayList<>();
        for (Product p : goods) {
            if (p.getTitle().toLowerCase().contains(search.toLowerCase())) {
                searchGoods.add(p);
            } else if (p.getDescriptionPromo().toLowerCase().contains(search.toLowerCase())) {
                searchGoods.add(p);
            } else if (p.getDescriptionFull().toLowerCase().contains(search.toLowerCase())) {
                searchGoods.add(p);
            } else if (p.getCategory().toLowerCase().contains(search.toLowerCase())) {
                searchGoods.add(p);
            } else if (p.getType().toLowerCase().contains(search.toLowerCase())) {
                searchGoods.add(p);
            } else if (p.getLinkToType().toLowerCase().contains(search)) {
                searchGoods.add(p);
            } else if (p.getLinkToCategory().toLowerCase().contains(search)) {
                searchGoods.add(p);
            }
        }
        return searchGoods;
    }

    private static User activeUser(Principal principal, UserService userService) {
        return userService.findByEmail(principal.getName());
    }
}
