package ru.dfr.springprojectstore.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.dfr.springprojectstore.entity.Product;
import ru.dfr.springprojectstore.entity.User;
import ru.dfr.springprojectstore.service.ProductService;
import ru.dfr.springprojectstore.service.PurchaseService;
import ru.dfr.springprojectstore.service.UserService;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class CategoryController {

    private final UserService userService;
    private final ProductService productService;
    private final PurchaseService purchaseService;

    // Отдых и развлечения
    @GetMapping("/recreation-and-entertainment")
    public String getRecreationAndEntertainment(Model model, Principal principal) {
        return outputCategoryLink("recreation-and-entertainment", "users", model, principal, userService, productService);
    }

    // Офис и сеть
    @GetMapping("/office-and-net")
    public String getOfficeAndNet(Model model, Principal principal) {
        return outputCategoryLink("office-and-net", "users", model, principal, userService, productService);
    }

    // Компьютеры
    @GetMapping("/computers")
    public String getComputers(Model model, Principal principal) {
        return outputCategoryLink("computers", "users", model, principal, userService, productService);
    }

    // ТВ и мультимедиа
    @GetMapping("/tv-and-multimedia")
    public String getTVAndMultimedia(Model model, Principal principal) {
        return outputCategoryLink("tv-and-multimedia", "users", model, principal, userService, productService);
    }

    // Бытовая техника
    @GetMapping("/appliances")
    public String getAppliances(Model model, Principal principal) {
        return outputCategoryLink("appliances", "users", model, principal, userService, productService);
    }

    // Смартфоны и гаджеты
    @GetMapping("/smartphones-and-gadgets")
    public String getSmartphonesAndGadgets(Model model, Principal principal) {
        return outputCategoryLink("smartphones-and-gadgets", "users", model, principal, userService, productService);
    }

    // вспомогательные методы
    private static String outputCategoryLink(String urlCategory, String page, Model model, Principal principal,
                                             UserService userService, ProductService productService) {
        model.addAttribute("countAllProducts", productService.findByIsActive(1).size());
        model.addAttribute("counts", productService.countsGoodsByCategory(productService.findByIsActive(1)));
        model.addAttribute("categories", productService.categoryAndUrl(productService.findByIsActive(1)));
        model.addAttribute("types", productService.typeAndUrl(clearListCategory(urlCategory, productService)));
        model.addAttribute("goods", clearListCategory(urlCategory, productService));
        model.addAttribute("user", activeUser(principal, userService));
        return page;
    }

    private static List<Product> clearListCategory(String urlCategory, ProductService productService) {
        List<Product> clearList = new ArrayList<>();
        for (Product product :  productService.findByIsActive(1)) {
            if (product.getLinkToCategory().equals("/" + urlCategory)) {
                clearList.add(product);
            }
        }
        return clearList;
    }

    private static User activeUser(Principal principal, UserService userService) {
        return userService.findByEmail(principal.getName());
    }
}
