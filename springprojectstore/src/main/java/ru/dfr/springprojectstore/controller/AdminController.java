package ru.dfr.springprojectstore.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.dfr.springprojectstore.entity.Product;
import ru.dfr.springprojectstore.entity.Purchase;
import ru.dfr.springprojectstore.entity.User;
import ru.dfr.springprojectstore.service.ProductService;
import ru.dfr.springprojectstore.service.PurchaseService;
import ru.dfr.springprojectstore.service.UserService;

import java.util.Comparator;
import java.util.stream.Collectors;


@Controller
@RequiredArgsConstructor
public class AdminController {

    private final ProductService productService;
    private final UserService userService;
    private final PurchaseService purchaseService;

    @GetMapping("/admin")
    public String getAdminPage(Model model) {
        return sizeModels("admin", model, userService, productService, purchaseService);
    }

    @GetMapping("/admin/users")
    public String getAdminByUsers(Model model) {
        model.addAttribute("users", userService.findAll().stream().sorted(Comparator.comparing(User::getId)).collect(Collectors.toList()));
        return sizeModels("users", model, userService, productService, purchaseService);
    }

    @GetMapping("/admin/goods")
    public String getAdminByGoods(Model model) {
        model.addAttribute("goods", productService.findAll().stream().sorted(Comparator.comparing(Product::getId)).collect(Collectors.toList()));
        return sizeModels("goods", model, userService, productService, purchaseService);
    }

    @GetMapping("/admin/purchases")
    public String getAdminByPurchases(Model model) {
        model.addAttribute("purchases", purchaseService.getPurchases().stream().sorted(Comparator.comparing(Purchase::getId)).collect(Collectors.toList()));
        return sizeModels("purchases", model, userService, productService, purchaseService);
    }

    private static String sizeModels(String url, Model model, UserService userService, ProductService productService, PurchaseService purchaseService) {
        model.addAttribute("usersCount", userService.findAll().size());
        model.addAttribute("goodsCount", productService.findAll().size());
        model.addAttribute("purchasesCount", purchaseService.getPurchases().size());
        return "/admin/" + url;
    }
}
