package ru.dfr.springprojectstore.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.dfr.springprojectstore.entity.Product;
import ru.dfr.springprojectstore.entity.User;
import ru.dfr.springprojectstore.service.ProductService;
import ru.dfr.springprojectstore.service.PurchaseService;
import ru.dfr.springprojectstore.service.UserService;

import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class BasketController {

    private final UserService userService;
    private final ProductService productService;
    private final PurchaseService purchaseService;

    // Корзина (отображение)
    @GetMapping("/basket/{userId}")
    public String getBasketPage(Model model, Principal principal, @PathVariable("userId") Integer userId) {
        if (userId != activeUser(principal, userService).getId() ) {
            return "/";
        } else if (productService.findAllByUserId(userId).size() == 0) {
            return "/basket_null";
        } else {
            model.addAttribute("goods", productService.findAllByUserId(userId));
            model.addAttribute("user", userId);
            model.addAttribute("size", productService.findAllByUserId(userId).size());
            return "/basket";
        }
    }

    @GetMapping("/basket_null")
    public String getBasketNull(Model model, Principal principal) {
        model.addAttribute("sizePurchases", purchaseService.listPurchasesByUserId(activeUser(principal, userService).getId()).size());
        return "/basket_null";
    }

    //  Удаление из корзины
    @PostMapping("/remove/{productId}")
    public String removeProductFromBasket(Principal principal, @PathVariable("productId") Integer productId) {
        Product product = productService.findById(productId);
        Integer oldOrders = product.getOrders();
        Integer oldQuantity = product.getQuantity();
        Integer newOrders = 0;
        Integer newQuantity = oldOrders + oldQuantity;
        product.setQuantity(newQuantity);
        product.setOrders(newOrders);
        product.setUser(userService.findById(10));
        productService.saveProduct(product);
        return "redirect:/basket/" + activeUser(principal, userService).getId();
    }

    // вспомогательные методы
    private static User activeUser(Principal principal, UserService userService) {
        return userService.findByEmail(principal.getName());
    }
}
