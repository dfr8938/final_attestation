package ru.dfr.springprojectstore.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.dfr.springprojectstore.entity.Product;
import ru.dfr.springprojectstore.entity.Purchase;
import ru.dfr.springprojectstore.entity.User;
import ru.dfr.springprojectstore.service.ProductService;
import ru.dfr.springprojectstore.service.PurchaseService;
import ru.dfr.springprojectstore.service.UserService;
import ru.dfr.springprojectstore.view.ProductView;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;
    private final UserService userService;
    private final PurchaseService purchaseService;

    @GetMapping("/goods")
    public String getUpdateGoodsPage(Model model) {
        model.addAttribute("goods", productService.findAll());
        model.addAttribute("categories", productService.findAll().stream().map(Product::getCategory).distinct().collect(Collectors.toList()));
        model.addAttribute("types", productService.findAll().stream().map(Product::getType).distinct().collect(Collectors.toList()));
        model.addAttribute("linkToCategory", productService.findAll().stream().map(Product::getLinkToCategory).distinct().collect(Collectors.toList()));
        model.addAttribute("linkToType", productService.findAll().stream().map(Product::getLinkToType).distinct().collect(Collectors.toList()));
        return "/goods";
    }

    @GetMapping("/")
    public String getProductsPage(Model model, Principal principal) {
        model.addAttribute("countAllProducts", productService.findByIsActive(1).size());
        model.addAttribute("counts", productService.countsGoodsByCategory(productService.findByIsActive(1)));
        model.addAttribute("categories", productService.categoryAndUrl(productService.findByIsActive(1)));
        model.addAttribute("types", productService.typeAndUrl(productService.findByIsActive(1)));
        model.addAttribute("goods", productService.findByIsActive(1));
        model.addAttribute("user", activeUser(principal, userService));
        return "users";
    }

    @PostMapping("/goods")
    public String createProduct(Principal principal, Model model, ProductView productView) {
        User user = activeUser(principal, userService);
        Product product = Product.builder()
                .title(productView.getTitle())
                .descriptionFull(productView.getDescriptionFull())
                .descriptionPromo(productView.getDescriptionPromo())
                .price(productView.getPrice())
                .quantity(productView.getQuantity())
                .urlImage(productView.getUrlImage())
                .category(productView.getCategory())
                .type(productView.getType())
                .linkToCategory(productView.getLinkToCategory())
                .linkToType(productView.getLinkToType())
                .isActive(productView.getIsActive())
                .build();
        product.setUser(user);
        product.setOrders(0);
        productService.saveProduct(product);
        model.addAttribute("user", user);
        return "redirect:/goods";
    }

    @PostMapping("/admin/goods/{productId}/update")
    public String updateProduct(Principal principal, Model model, @PathVariable("productId") Integer productId, ProductView productView) {
        User user = activeUser(principal, userService);
        Product product = productService.findById(productId);
        product.setTitle(productView.getTitle());
        product.setDescriptionFull(productView.getDescriptionFull());
        product.setDescriptionPromo(productView.getDescriptionPromo());
        if (productView.getPrice() == null) {
            product.setPrice(product.getPrice());
        } else {
            product.setPrice(productView.getPrice());
        }
        product.setQuantity(product.getQuantity());
        product.setUrlImage(productView.getUrlImage());
        product.setCategory(productView.getCategory());
        product.setType(productView.getType());
        product.setLinkToCategory(productView.getLinkToCategory());
        product.setLinkToType(productView.getLinkToType());
        product.setIsActive(productView.getIsActive());
        product.setUser(user);
        product.setOrders(0);
        productService.saveProduct(product);
        model.addAttribute("user", user);
        return "redirect:/admin/goods";
    }

    // Удаление товара
    @PostMapping("/goods/{productId}/delete")
    public String deleteProduct(@PathVariable("productId") Integer productId) {
        return removeProduct("goods", productId, purchaseService, productService);
    }

    @PostMapping("/admin/goods/{productId}/delete")
    public String deleteProduct(@PathVariable("productId") Integer productId, @RequestParam("not_title") String title) {
        return removeProduct("admin/goods", productId, purchaseService, productService);
    }

    @GetMapping("/goods/{productId}")
    public String getProductsPageAdmin(@PathVariable("productId") Integer productId, Model model, Principal principal) {
        model.addAttribute("user",activeUser(principal, userService));
        model.addAttribute("product", productService.findById(productId));
        return "/product";
    }

    // Удаление товара в карточке товара
    @PostMapping("/smartphones-and-gadgets/smartphones/{productId}/delete")
    public String deleteSmartphone(@PathVariable("productId") Integer productId) {
        productService.removeProduct(productId);
        return "redirect:/";
    }

    @PostMapping("/recreation-and-entertainment/unicycles/{productId}/delete")
    public String deleteUnicycle(@PathVariable("productId") Integer productId) {
        productService.removeProduct(productId);
        return "redirect:/";
    }

    @PostMapping("/tv-and-multimedia/tv/{productId}/delete")
    public String deleteTV(@PathVariable("productId") Integer productId) {
        productService.removeProduct(productId);
        return "redirect:/";
    }

    @PostMapping("/smartphones-and-gadgets/cellular-phones/{productId}/delete")
    public String deleteCellularPhone(@PathVariable("productId") Integer productId) {
        productService.removeProduct(productId);
        return "redirect:/";
    }

    @PostMapping("/appliances/washing-machines/{productId}/delete")
    public String deleteWashingMachine(@PathVariable("productId") Integer productId) {
        productService.removeProduct(productId);
        return "redirect:/";
    }

    @PostMapping("/computers/notebooks/{productId}/delete")
    public String deleteNotebook(@PathVariable("productId") Integer productId) {
        productService.removeProduct(productId);
        return "redirect:/";
    }

    @PostMapping("/computers/monoblocks/{productId}/delete")
    public String deleteMonoblock(@PathVariable("productId") Integer productId) {
        productService.removeProduct(productId);
        return "redirect:/";
    }

    @PostMapping("/office-and-net/mfu/{productId}/delete")
    public String deleteMFU(@PathVariable("productId") Integer productId) {
        productService.removeProduct(productId);
        return "redirect:/";
    }

    // Добавление в корзину
    @PostMapping("/recreation-and-entertainment/unicycles/{productId}")
    public String addUnicycles(Principal principal,
                                 @PathVariable("productId") Integer productId,
                                 @RequestParam("order") Integer orderNum) {
        productService.addProductToUser(activeUser(principal, userService).getId(), productId, orderNum);
        return "redirect:/basket/" + activeUser(principal, userService).getId();
    }

    @PostMapping("/smartphones-and-gadgets/smartphones/{productId}")
    public String addSmartphones(Principal principal,
                                 @PathVariable("productId") Integer productId,
                                 @RequestParam("order") Integer orderNum) {
        productService.addProductToUser(activeUser(principal, userService).getId(), productId, orderNum);
        return "redirect:/basket/" + activeUser(principal, userService).getId();
    }

    @PostMapping("/tv-and-multimedia/tv/{productId}")
    public String addTV(Principal principal,
                        @PathVariable("productId") Integer productId,
                        @RequestParam("order") Integer orderNum) {
        productService.addProductToUser(activeUser(principal, userService).getId(), productId, orderNum);
        return "redirect:/basket/" + activeUser(principal, userService).getId();
    }

    @PostMapping("/smartphones-and-gadgets/cellular-phones/{productId}")
    public String addCellularPhones(Principal principal,
                                    @PathVariable("productId") Integer productId,
                                    @RequestParam("order") Integer orderNum) {
        productService.addProductToUser(activeUser(principal, userService).getId(), productId, orderNum);
        return "redirect:/basket/" + activeUser(principal, userService).getId();
    }

    @PostMapping("/appliances/washing-machines/{productId}")
    public String addWashingMachines(Principal principal,
                                     @PathVariable("productId") Integer productId,
                                     @RequestParam("order") Integer orderNum) {
        productService.addProductToUser(activeUser(principal, userService).getId(), productId, orderNum);
        return "redirect:/basket/" + activeUser(principal, userService).getId();
    }

    @PostMapping("/computers/notebooks/{productId}")
    public String addNotebooks(Principal principal,
                               @PathVariable("productId") Integer productId,
                               @RequestParam("order") Integer orderNum) {
        productService.addProductToUser(activeUser(principal, userService).getId(), productId, orderNum);
        return "redirect:/basket/" + activeUser(principal, userService).getId();
    }

    @PostMapping("/computers/monoblocks/{productId}")
    public String addMonoblocks(Principal principal,
                                @PathVariable("productId") Integer productId,
                                @RequestParam("order") Integer orderNum) {
        productService.addProductToUser(activeUser(principal, userService).getId(), productId, orderNum);
        return "redirect:/basket/" + activeUser(principal, userService).getId();
    }

    @PostMapping("/office-and-net/mfu/{productId}")
    public String addMFU(Principal principal,
                         @PathVariable("productId") Integer productId,
                         @RequestParam("orders") Integer orderNum) {
        productService.addProductToUser(activeUser(principal, userService).getId(), productId, orderNum);
        return "redirect:/basket/" + activeUser(principal, userService).getId();
    }

    // /recreation-and-entertainment/unicycles/id
    @GetMapping("/recreation-and-entertainment/unicycles/{productId}")
    public String getUnicycles(Model model, @PathVariable("productId") Integer productId, Principal principal) {
        return outputOnPageProduct(productId, model, principal, userService, productService);
    }

    // /smartphones-and-gadgets/smartphones/id
    @GetMapping("/smartphones-and-gadgets/smartphones/{productId}")
    public String getSmartphone(Model model, @PathVariable("productId") Integer productId, Principal principal) {
        return outputOnPageProduct(productId, model, principal, userService, productService);
    }

    // /office-and-net/mfu/id
    @GetMapping("/office-and-net/mfu/{productId}")
    public String getMFU(Model model, @PathVariable("productId") Integer productId, Principal principal) {
        return outputOnPageProduct(productId, model, principal, userService, productService);
    }

    // /smartphones-and-gadgets/cellular-phones/id
    @GetMapping("/smartphones-and-gadgets/cellular-phones/{productId}")
    public String getCellularPhone(Model model, @PathVariable("productId") Integer productId, Principal principal) {
        return outputOnPageProduct(productId, model, principal, userService, productService);
    }

    // /computers/notebooks/id
    @GetMapping("/computers/notebooks/{productId}")
    public String getNotebook(Model model, @PathVariable("productId") Integer productId, Principal principal) {
        return outputOnPageProduct(productId, model, principal, userService, productService);
    }

    // /appliances/washing-machines/id
    @GetMapping("/appliances/washing-machines/{productId}")
    public String getWashingMachine(Model model, @PathVariable("productId") Integer productId, Principal principal) {
        return outputOnPageProduct(productId, model, principal, userService, productService);
    }

    // /tv-and-multimedia/tv/id
    @GetMapping("/tv-and-multimedia/tv/{productId}")
    public String getTV(Model model, @PathVariable("productId") Integer productId, Principal principal) {
        return outputOnPageProduct(productId, model, principal, userService, productService);
    }

    // /computers/monoblocks/id
    @GetMapping("/computers/monoblocks/{productId}")
    public String getMonoblock(Model model, @PathVariable("productId") Integer productId, Principal principal) {
        return outputOnPageProduct(productId, model, principal, userService, productService);
    }

    // вспомогательные методы
    private static String outputOnPageProduct(Integer productId, Model model, Principal principal, UserService userService, ProductService productService) {
        Product product = productService.findById(productId);
        model.addAttribute("product", product);
        model.addAttribute("user", activeUser(principal, userService));
        return "/product";
    }

    private static User activeUser(Principal principal, UserService userService) {
        return userService.findByEmail(principal.getName());
    }

    private static List<Product> goodsNotOrders(Integer userId, ProductService productService) {
        List<Product> goods = productService.findAllByUserId(userId);
        List<Product> goodsNotOrders = new ArrayList<>();
        for (int i = 0; i < goods.size(); i++) {
            if (goods.get(i).getOrders() != 0) {
                goodsNotOrders.add(goods.get(i));
            }
        }
        return goodsNotOrders;
    }

    private static String removeProduct(String url, Integer productId, PurchaseService purchaseService, ProductService productService) {
        List<Purchase> purchases = purchaseService.listPurchasesByProductId(productId);
        for (int i = 0; i < purchases.size(); i++) {
            purchaseService.deletePurchase(purchases.get(i).getId());
        }
        productService.removeProduct(productId);
        return "redirect:/admin/goods";
    }
}
