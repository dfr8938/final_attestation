package ru.dfr.springprojectstore.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.dfr.springprojectstore.entity.User;
import ru.dfr.springprojectstore.service.SignUpService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
@RequiredArgsConstructor
@RequestMapping("/signUp")
public class SignUpController {

    private final SignUpService signUpService;

    @GetMapping
    public String getSignUpPage() {
        return "/signUp";
    }
    @PostMapping
    public String signUpUser(User user) {
        if (user.getEmail().equals("") || user.getPassword().equals("")
                || user.getFirstName().equals("") || user.getLastName().equals("")) {
            return "redirect:/signUp";
        }

        Pattern emailPattern = Pattern
                .compile("^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$");
        Matcher emailMatcher = emailPattern.matcher(user.getEmail());

        if (emailMatcher.matches()) {
            signUpService.signUpUser(user);
            return "redirect:/signIn";
        } else {
            return "redirect:/signUp";
        }
    }
}
