package ru.dfr.springprojectstore.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.dfr.springprojectstore.entity.Product;
import ru.dfr.springprojectstore.entity.Purchase;
import ru.dfr.springprojectstore.entity.User;
import ru.dfr.springprojectstore.service.ProductService;
import ru.dfr.springprojectstore.service.PurchaseService;
import ru.dfr.springprojectstore.service.UserService;

import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class PurchaseController {

    private final PurchaseService purchaseService;
    private final ProductService productService;
    private final UserService userService;

    @PostMapping("/basket/{userId}/save")
    public String savePurchase(Principal principal,
                               @RequestParam("productId") Integer productId,
                               @PathVariable("userId") Integer userId) {
        User user = userService.findById(userId);
        Product product = productService.findById(productId);
        Purchase purchase = Purchase.builder()
                .product(product)
                .user(user)
                .orders(product.getOrders())
                .build();
        purchaseService.savePurchase(purchase);
        product.setUser(userService.findById(10));
        product.setQuantity(product.getQuantity());
        product.setOrders(0);
        productService.saveProduct(product);
        return "redirect:/basket/" + userId;
    }

    @PostMapping("/admin/purchases/{purchaseId}/delete")
    public String removePurchase(@PathVariable("purchaseId") Integer purchaseId) {
        purchaseService.deletePurchase(purchaseId);
        return "redirect:/admin/purchases";
    }

    private static User activeUser(Principal principal, UserService userService) {
        return userService.findByEmail(principal.getName());
    }
}
