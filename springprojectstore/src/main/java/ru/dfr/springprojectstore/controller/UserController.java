package ru.dfr.springprojectstore.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.dfr.springprojectstore.entity.Product;
import ru.dfr.springprojectstore.entity.Purchase;
import ru.dfr.springprojectstore.entity.User;
import ru.dfr.springprojectstore.security.details.UserDetailsServiceImpl;
import ru.dfr.springprojectstore.service.ProductService;
import ru.dfr.springprojectstore.service.PurchaseService;
import ru.dfr.springprojectstore.service.UserService;
import ru.dfr.springprojectstore.view.UserView;

import javax.websocket.server.PathParam;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final ProductService productService;
    private final UserDetailsServiceImpl userDetailsServiceImpl;
    private final UserView userView;
    private final PasswordEncoder passwordEncoder;
    private final PurchaseService purchaseService;

    @GetMapping("/users")
    public String findAll(Model model) {
        List<User> users = userService.findAll();
        model.addAttribute("users", users);
        return "/users";
    }

    @PostMapping("/users")
    public String createUser(User user) {
        userService.saveUser(user);
        return "redirect:/users";
    }

    @GetMapping("/users/{userId}")
    public String findById(Model model, @PathVariable("userId") Integer userId, Principal principal) {
        User user = userService.findById(userId);
        int id = userService.findByEmail(principal.getName()).getId();
        if (userId != activeUser(principal, userService).getId()) {
            return "/";
        } else {
            Map<Product, Integer> goods = new HashMap<>();
            List<Integer> orders = purchaseService.listPurchasesByUserId(userId).stream().map(Purchase::getOrders).collect(Collectors.toList());
            List<Product> purchases = purchaseService.listPurchasesByUserId(userId)
                    .stream().map(Purchase::getProduct).collect(Collectors.toList());
            for (int i = 0; i < orders.size(); i++) {
                if (!goods.containsKey(purchases.get(i))) {
                    goods.put(purchases.get(i), orders.get(i));
                } else {
                    goods.put(purchases.get(i), goods.get(purchases.get(i)) + orders.get(i));
                }
            }
            if (purchases.size() == 0) {
                model.addAttribute("user", user);
                return "/not_purchases";
            } else {
                model.addAttribute("user", user);
                model.addAttribute("purchases", goods);
            }
            return "/user";
        }
    }

    @PostMapping("/users/{userId}/edit")
    public String editUser(@PathVariable("userId") Integer userId, UserView userView) {
        User user = userService.findById(userId);

        Pattern emailPattern = Pattern
                .compile("^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$");
        Matcher emailMatcher = emailPattern.matcher(userView.getEmail());

        if (emailMatcher.matches()) {
            user.setFirstName(userView.getFirstName());
            user.setLastName(userView.getLastName());
            user.setEmail(userView.getEmail());
            userService.saveUser(user);
            return "redirect:/users/" + userId;
        } else {
            return "redirect:/users";
        }
    }

    @PostMapping("/admin/users/{userId}/edit")
    public String editUser(@PathVariable("userId") Integer userId, @RequestParam("email") String email,
                                @RequestParam("lastName") String lastName, @RequestParam("firstName") String firstName) {
        User user = userService.findById(userId);

        Pattern emailPattern = Pattern
                .compile("^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$");
        Matcher emailMatcher = emailPattern.matcher(email);

        if (emailMatcher.matches()) {
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            userService.saveUser(user);
            return "redirect:/admin/users";
        } else {
            return "redirect:/admin/users";
        }
    }

    @PostMapping("/users/{userId}/delete")
    public String deleteUser(@PathVariable("userId") Integer userId) {
        userService.deleteById(userId);
        return "redirect:/users";
    }

    @PostMapping("/admin/users/{userId}/delete")
    public String deleteUser(@PathVariable("userId") Integer userId, @RequestParam("email") String email) {
        List<Purchase> purchases = purchaseService.listPurchasesByUserId(userId);
        if (purchases.size() > 0) {
            for (int i = 0; i < purchases.size(); i++) {
                purchaseService.deletePurchase(purchases.get(i).getId());
            }
        }
        userService.deleteById(userId);
        return "redirect:/admin/users";
    }

    private static User activeUser(Principal principal, UserService userService) {
        return userService.findByEmail(principal.getName());
    }
}
