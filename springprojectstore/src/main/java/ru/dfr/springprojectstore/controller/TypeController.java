package ru.dfr.springprojectstore.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ru.dfr.springprojectstore.entity.Product;
import ru.dfr.springprojectstore.entity.User;
import ru.dfr.springprojectstore.service.ProductService;
import ru.dfr.springprojectstore.service.PurchaseService;
import ru.dfr.springprojectstore.service.UserService;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class TypeController {

    private final UserService userService;
    private final ProductService productService;
    private final PurchaseService purchaseService;

    // Отдых и развлечения (моноколеса)
    @GetMapping("/recreation-and-entertainment/unicycles")
    public String getRecreationAndEntertainmentByUnicycles(Model model, Principal principal) {
        return outputTypeLink("recreation-and-entertainment", "unicycles", "users", model, principal, userService, productService);
    }

    // Смартфоны и гаджеты (смартфоны)
    @GetMapping("/smartphones-and-gadgets/smartphones")
    public String getSmartphonesAndGadgetsBySmartphone(Model model, Principal principal) {
        return outputTypeLink("smartphones-and-gadgets", "smartphones", "users", model, principal, userService, productService);
    }

    // Смартфоны и гаджеты (сотовые телефоны)
    @GetMapping("/smartphones-and-gadgets/cellular-phones")
    public String getSmartphonesAndGadgetsByCellularPhone(Model model, Principal principal) {
        return outputTypeLink("smartphones-and-gadgets", "cellular-phones", "users", model, principal, userService, productService);
    }

    // Бытовая техника (стиральные машины)
    @GetMapping("/appliances/washing-machines")
    public String getAppliancesByWashingMachine(Model model, Principal principal) {
        return outputTypeLink("appliances", "washing-machines", "users", model, principal, userService, productService);
    }

    // ТВ и мультимедиа (ТВ)
    @GetMapping("/tv-and-multimedia/tv")
    public String getTVAndMultimediaByTV(Model model, Principal principal) {
        return outputTypeLink("tv-and-multimedia", "tv", "users", model, principal, userService, productService);
    }

    // Компьютеры (Ноутбуки)
    @GetMapping("/computers/notebooks")
    public String getComputersByNotebook(Model model, Principal principal) {
        return outputTypeLink("computers", "notebooks", "users", model, principal, userService, productService);
    }

    // Компьютеры (Моноблоки)
    @GetMapping("/computers/monoblocks")
    public String getComputersByMonoblock(Model model, Principal principal) {
        return outputTypeLink("computers", "monoblocks", "users", model, principal, userService, productService);
    }

    // Офис и сеть (МФУ)
    @GetMapping("/office-and-net/mfu")
    public String getOfficeAndNetByMFU(Model model, Principal principal) {
        return outputTypeLink("office-and-net", "mfu", "users", model, principal, userService, productService);
    }

    // вспомогательные методы
    private static String outputTypeLink(String urlCategory, String urlType, String page, Model model, Principal principal,
                                         UserService userService, ProductService productService) {
        model.addAttribute("countAllProducts", productService.findByIsActive(1).size());
        model.addAttribute("counts", productService.countsGoodsByCategory(productService.findByIsActive(1)));
        model.addAttribute("categories", productService.categoryAndUrl(productService.findByIsActive(1)));
        model.addAttribute("types", productService.typeAndUrl(clearListCategoryAndType(urlCategory, urlType, productService)));
        model.addAttribute("goods", clearListCategoryAndType(urlCategory, urlType, productService));
        model.addAttribute("user", activeUser(principal, userService));
        return page;
    }

    private static List<Product> clearListCategoryAndType(String urlCategory, String urlType, ProductService productService) {
        List<Product> clearList = new ArrayList<>();
        for (Product product : productService.findByIsActive(1)) {
            if (product.getLinkToCategory().equals("/" + urlCategory)
                    && product.getLinkToType().equals("/" + urlType)) {
                clearList.add(product);
            }
        }
        return clearList;
    }

    private static User activeUser(Principal principal, UserService userService) {
        return userService.findByEmail(principal.getName());
    }
}
