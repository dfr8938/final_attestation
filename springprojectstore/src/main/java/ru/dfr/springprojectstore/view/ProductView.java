package ru.dfr.springprojectstore.view;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.dfr.springprojectstore.entity.User;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductView {
    private Integer id;
    private String title;
    private String descriptionFull;
    private String descriptionPromo;
    private Integer price;
    private String urlImage;
    private Integer quantity;
    private Integer orders;
    private String category;
    private String type;
    private String linkToCategory;
    private String linkToType;

    private Integer isActive = 1;

    private User user;
}
