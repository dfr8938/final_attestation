package ru.dfr.springprojectstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.dfr.springprojectstore.entity.Product;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    List<Product> findProductByPriceAfterAndPriceBeforeOrderByPrice(Integer price, Integer price2);
    List<Product> findAllByUser_Id(Integer id);
    List<Product> findProductByPriceAfterAndPriceBeforeAndCategory(Integer price, Integer price2, String category);
    List<Product> findByTitleEndsWith(String str);
    List<Product> findByIsActive(Integer isActive);
}
