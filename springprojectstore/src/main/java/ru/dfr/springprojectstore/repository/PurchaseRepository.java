package ru.dfr.springprojectstore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.dfr.springprojectstore.entity.Product;
import ru.dfr.springprojectstore.entity.Purchase;

import java.util.List;
import java.util.Map;

public interface PurchaseRepository extends JpaRepository<Purchase, Integer> {
    List<Purchase> findAllByUser_Id(Integer id);
    List<Purchase> findAllByProduct_Id(Integer id);
}
