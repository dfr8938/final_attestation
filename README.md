# Итоговая работа в рамках курса "Введение в разработку корпоративных приложений на Java" АНО ДПО "Университет иннополис"

## Описание

Проект представляет собой интернет-магазин с возможностью регистрации/авторизации пользователя, фильтрации товаров по категориям и типам категорий, наличием личного кабинета пользователя с отображением истории покупок данного пользователя и возможностью изменения данным пользователем своих данных. В проекте также реализованна корзина и административная панель.

## Технологии

>## Back-end:
>* [Java 11](https://www.oracle.com/java/technologies/downloads/#java11)
>* [Maven](https://maven.apache.org/)
>* [Spring Boot](https://github.com/spring-projects/spring-boot)
>* [Spring DATA JPA](https://github.com/spring-projects/spring-data-jpa)
>* [Spring Security](https://github.com/spring-projects/spring-security)
>* [Lombok](https://github.com/projectlombok/lombok)
>* [PostgreSQL](https://github.com/postgres)

>## Front-end:
>* HTML5
>* CSS3
>* [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
>* [Freemaker](https://freemarker.apache.org/)
